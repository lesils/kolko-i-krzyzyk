<?php
class board {
    public $player1;
    public $player2;
    public $elements = array();
    function __construct() {
        for ($i = 0; $i < 9; $i++) {
            $this->elements[$i] = $i+1;
        }
    }

    function displayBoard() {
        echo "<div id='ttcBoard'><table>".
            "<tr>".
            "<td>".$this->elements[0]."</td>".
            "<td>".$this->elements[1]."</td>".
            "<td>".$this->elements[2]."</td>".
            "</tr>".
            "<tr>".
            "<td>".$this->elements[3]."</td>".
            "<td>".$this->elements[4]."</td>".
            "<td>".$this->elements[5]."</td>".
            "</tr>".
            "<tr>".
            "<td>".$this->elements[6]."</td>".
            "<td>".$this->elements[7]."</td>".
            "<td>".$this->elements[8]."</td>".
            "</tr>".
            "</table></div>";
    }

    function checkForWin($player) {
        if (($this->elements[0] == $player && $this->elements[1] == $player && $this->elements[2] == $player) ||
            ($this->elements[3] == $player && $this->elements[4] == $player && $this->elements[5] == $player) ||
            ($this->elements[6] == $player && $this->elements[7] == $player && $this->elements[8] == $player) ||
            ($this->elements[0] == $player && $this->elements[3] == $player && $this->elements[6] == $player) ||
            ($this->elements[1] == $player && $this->elements[4] == $player && $this->elements[7] == $player) ||
            ($this->elements[2] == $player && $this->elements[5] == $player && $this->elements[8] == $player) ||
            ($this->elements[0] == $player && $this->elements[4] == $player && $this->elements[8] == $player) ||
            ($this->elements[6] == $player && $this->elements[4] == $player && $this->elements[2] == $player)
        ) {
            echo "<br/>Gracz ".$player." wygral!<br/>";
            session_destroy();
            echo "<a href='start.php'>Wystartuj nowa gre</a>";
            exit;
        }
        elseif ($_SESSION['turn'] == 9) {
            echo "<br/>Ups, niestety remis!<br/>";
            echo "<br/><a href='start.php'>Wystartuj nowa gre</a></br>";
            session_destroy();

            printf( '<img src="draw.jpg">');
            exit;
        }
    }

    function getCurrentPlayer($turn) {
        if ($turn % 2 == 0) {
                        echo "Aktywny gracz 2: " . $this->player2;
        } else {
                        echo "Aktrywny gracz 1: " . $this->player1;
        }
    }

    function assignTakenTurn($turn, $choice) {
        if ($turn % 2 == 0) {
            $this->elements[$choice - 1] = $this->player2;
            $this->checkForWin($this->player2);
        } else {
            $this->elements[$choice - 1] = $this->player1;
            $this->checkForWin($this->player1);
        }
    }
}
