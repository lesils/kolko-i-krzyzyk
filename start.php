<?php
session_start();
require("board.php");
?>
<!DOCTYPE html>

    <head>
        <meta charset="UTF-8"/>
        <title>Kolko i krzyzyk</title>
        <link href="Styles.css" type="text/css" rel="stylesheet"/>
    </head>
    <body>
        <div id="main">
        <h1>Kolko i Krzyzyk</h1>
        <?php



        if (isset($_GET['submit1'])) {
            if (isset($_GET['piece'])) {
                $board = new board();
                $board->player1 = $_GET['piece'];
                $_SESSION['turn'] = 0;
                if ($board->player1 == 'X') {//przypisanie znaku dla  gracza
                    $board->player2 = 'O';
                } else {
                    $board->player2 = 'X';
                }
                $_SESSION['board'] = serialize($board);
            } else { //jezeli nie wybierze to przejdzie do wiadomosci
                echo "prosze wybrac znak.";
                echo "<form action='start.php'>";
                echo "<input type='submit' value='Wroc!' />";
                echo "</form>";
            }
        }
        elseif (!isset($_GET['submit2'])) {
            // poczatek i przygotowanie gry
                       $_SESSION['turn'] = '';
            ?>
            <form method="get" action="start.php">
                <input type="radio" name="piece" value="X"/> X <br/>
                <input type="radio" name="piece" value="O"/> O <br/>
                <input type="submit" name="submit1" value="graczu, wybierz O lub X!"/>
            </form>
        <?php
        }

        // pocz�tek rozgrywki
        if (is_numeric($_SESSION['turn'])) {

            $board = unserialize($_SESSION['board']);

            if (isset($_GET['submit2'])) {
                if (isset($_GET['choice'])) {// zapisanie rozgrywki

                    $board->assignTakenTurn($_SESSION['turn'], $_GET['choice']);

                    $_SESSION['turn']++;
                    $board->getCurrentPlayer($_SESSION['turn']);

                    echo "<br/>Tura numer: " . $_SESSION['turn'];

                    $board->displayBoard();

                    $_SESSION['board'] = serialize($board);
                } else {
                    echo "Wybierz pole na dole ekranu.<br/>Turn: " . $_SESSION['turn'] . "<br/>";
                    $board->getCurrentPlayer($_SESSION['turn']);
                    $board->displayBoard();
                }
            }
            else {
                        // gra jeszcze nie ruszyla
                         if ($_SESSION['turn'] == 0) {
                             $_SESSION['turn']++;
                           echo "kolejka gracza nr 1: ".$board->player1;
                           echo "<br/>Tura: " . $_SESSION['turn'];
                          $board->displayBoard();
                }
            }

            echo "<form method='get' action='start.php'>";
            foreach ($board->elements as $space) {
                if (is_numeric($space)) {
                    echo "<input type='radio' name='choice' value='$space'/> $space <br/>";
                }
            }
            echo "<input type='submit' name='submit2' value='Wykonaj swoh ruch!'/>";
            echo "</form>";
        }
        ?>
        </div>
    </body>
</html>
